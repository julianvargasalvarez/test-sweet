class Repo < ActiveRecord::Base
  attr_accessible :name, :owner, :url

  validates :name, presence: true, uniqueness: true
  validates :url, presence: true, uniqueness: true,
    format: { with: /git@\w+\.\w{2,3}\:\w+\/\w+/ }

  belongs_to :user
  has_many :commits
end
