require 'spec_helper'

feature 'home' do
  it 'muestra el mensaje "hola mundo"' do
    visit root_path
    expect(page).to have_content("hola mundo")
  end

  it 'muestra los repositorios de la base de datos' do
    Repo.create(name: 'rails', owner: '-')
    Repo.create(name: 'ruby', owner: '-')

    visit root_path
    expect(page).to have_content("rails")
    expect(page).to have_content("ruby")
  end
end
