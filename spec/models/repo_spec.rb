require 'spec_helper'

describe Repo do
  it { should validate_presence_of(:name) }

  it { should validate_presence_of(:url) }

  it { should validate_uniqueness_of(:name) }

  it { should validate_uniqueness_of(:url) }

  it { should allow_value('git@github.com:julianvargasalvarez/Cosmos03').for(:url) }

  it { should_not allow_value('hola mundo').for(:url) }

  it { should belong_to(:user) }

  it { should have_many(:commits) }
end
