class AddOwnerToRepos < ActiveRecord::Migration
  def up
    add_column :repos, :owner, :string, null: true
  end

  def down
    remove_column :repos, :owner
  end
end
