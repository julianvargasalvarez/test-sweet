class AddsUrlToRepo < ActiveRecord::Migration
  def up
    add_column :repos, :url, :string
  end

  def down
    remove_column :repos, :url
  end
end
