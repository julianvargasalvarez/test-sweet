class CreateCommitsTabel < ActiveRecord::Migration
  def up
    create_table :commits do |t|
      t.integer :repo_id
      t.string :sha1
      t.timestamps
    end
  end

  def down
    drop_table :commits
  end
end
