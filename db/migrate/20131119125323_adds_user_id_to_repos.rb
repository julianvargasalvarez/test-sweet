class AddsUserIdToRepos < ActiveRecord::Migration
  def up
    add_column :repos, :user_id, :integer
  end

  def down
    remove_column :repos, :user_id
  end
end
