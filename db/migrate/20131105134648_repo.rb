class Repo < ActiveRecord::Migration
  def up
    create_table :repos do |t|
      t.string :name
      t.timestamps
    end
  end

  def down
    drop_table :repos
  end
end
